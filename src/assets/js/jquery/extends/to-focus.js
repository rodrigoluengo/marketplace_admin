import $ from 'jquery'
$.fn.extend({

  toFocus: function () {
    return this.each(function () {
      const scope = $(this)
      window.setTimeout(function () { scope.focus() }, 100)
    })
  }

})
