import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

import router from './modules/router'
import auth from './modules/auth'
import passwordReset from './modules/password-reset'
import category from './modules/category'
import item from './modules/item'
import person from './modules/person'
import federalReceiveDocument from './modules/federal-receive-document'
import address from './modules/address'

const store = new Vuex.Store({
  modules: {
    router,
    auth,
    passwordReset,
    category,
    item,
    person,
    federalReceiveDocument,
    address
  },
  plugins: [createPersistedState({
    key: 'marketplace',
    paths: ['router', 'auth']
  })]
})

export default store
