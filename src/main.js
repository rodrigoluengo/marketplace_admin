import './assets/scss/default.scss'

import Vue from './vue'
import App from './App'
import router from './router'
import store from './store'
import AuthInterceptor from './interceptors/auth'
import './directives/to-select'
import './directives/mask'
import './filters/number-format'

window.$ = window.jQuery = require('jquery/dist/jquery')
window._ = require('lodash')
require('jquery-ui-bundle')
require('bootstrap-sass/assets/javascripts/bootstrap')
require('bootstrap-notify')
require('jstree')
require('jstree/dist/themes/default/style.css')
require('summernote/dist/summernote.css')
require('summernote')
require('jquery-number')
require('meiomask/src/meiomask')
require('jquery-mask-plugin')

require('./assets/js/jquery/extends/to-focus')
require('./assets/js/jquery/extends/to-select')
require('./assets/js/bootstrap/fixes')
require('./assets/js/prototype/String')

Vue.config.productionTip = process.env.NODE_ENV === 'production'
Vue.http.interceptors.push(AuthInterceptor)
Vue.prototype['_'] = window._

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App },
  store
})
