import Vue from '@/vue'

const PasswordResetResource = Vue.resource(`${process.env.API}/password-reset{/id}`)

export default PasswordResetResource
