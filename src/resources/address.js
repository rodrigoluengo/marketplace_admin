import Vue from '@/vue'

const AddressResource = Vue.resource(`${process.env.API}/address{/id}`, {}, {
  zipCode: {method: 'GET', url: `${process.env.API}/address/zip-code`}
})

export default AddressResource
