import resource from '@/resources/category'
import { Dialog, Notify } from '@/assets/js/bootstrap/ui'

export const NEW = 'CATEGORY_NEW'
export const ERROR = 'CATEGORY_ERROR'
export const SAVE_SUCCESS = 'CATEGORY_SAVE_SUCCESS'
export const SELECT = 'CATEGORY_SELECT'
export const JSTREE_SUCCESS = 'JSTREE_SUCCESS'
export const EDIT = 'CATEGORY_EDIT'
export const REMOVE_SUCCESS = 'CATEGORY_REMOVE_SUCCESS'
export const SEARCH = 'CATEGORY_SEARCH'

export const initialValue = {
  category_id: null,
  name: null,
  status: true,
  details: null
}

const category = {
  namespaced: true,
  state: {
    search: null,
    category: initialValue,
    categories: null,
    jstree: null,
    errors: null
  },

  mutations: {
    [NEW] (state) {
      const newCategory = Object.assign({}, initialValue)
      const categoryId = state.category && 'id' in state.category ? state.category.id : null
      newCategory['category_id'] = categoryId
      state.category = newCategory
    },
    [ERROR] (state, payload) {
      state.errors = payload
    },
    [SAVE_SUCCESS] (state, payload) {
      state.category = Object.assign(state.category, payload)
    },
    [SELECT] (state, payload) {
      state.category = payload || initialValue
    },
    [JSTREE_SUCCESS] (state, payload) {
      state.jstree = payload
    },
    [EDIT] (state, payload) {
      state.category = payload
    },
    [REMOVE_SUCCESS] (state) {
      state.category = initialValue
    },
    [SEARCH] (state, payload) {
      state.search = payload
    }
  },

  actions: {
    _new ({commit}) {
      commit(NEW)
    },
    save ({commit, state}) {
      let promise
      if ('id' in state.category && state.category.id) {
        promise = resource.update({id: state.category.id}, state.category)
      } else {
        promise = resource.save({}, state.category)
      }
      promise.then(response => {
        commit(SAVE_SUCCESS, response.body)
        Notify.success('Categoria salva com sucesso')
      }, error => {
        commit(ERROR, error.body)
      })
    },
    jstree ({commit}) {
      resource.jstree().then(response => {
        commit(JSTREE_SUCCESS, response.body)
      })
    },
    select ({commit}, payload) {
      commit(SELECT, payload)
    },
    edit ({commit}, payload) {
      commit(EDIT, payload)
    },
    _move ({commit}, payload) {
      resource.move(payload).then(() => {
        Notify.success('Categoria movida com sucesso')
      }, error => {
        commit(ERROR, error.body)
      })
    },
    remove ({commit}, payload) {
      Dialog.confirm('Deseja realmente remover esta categoria?', yes => {
        resource.delete({id: payload.id}).then(() => {
          commit(REMOVE_SUCCESS)
          Notify.success('Categoria removida com sucesso')
        }, error => {
          commit(ERROR, error.body)
        })
      })
    },
    _search ({commit, state}, payload) {
      commit(SEARCH, payload)
    }
  }
}

export default category
