import asyncWaterfall from 'async-waterfall'
import _ from 'lodash'
import resource from '@/resources/item'
import fileResource from '@/resources/file'
import emptyDataSource from '@/assets/js/data/empty-data-source'
import { Notify } from '@/assets/js/bootstrap/ui'

export const NEW = 'ITEM_NEW'
export const ERROR = 'ITEM_ERROR'
export const SAVE_SUCCESS = 'ITEM_SAVE_SUCCESS'
export const SELECT = 'ITEM_SELECT'
export const EDIT = 'ITEM_EDIT'
export const REORDER = 'ITEM_REORDER'
export const REMOVE_SUCCESS = 'ITEM_REMOVE_SUCCESS'
export const SEARCH_SUCCESS = 'ITEM_SEARCH_SUCCESS'

export const initialValue = {
  category_id: null,
  name: null,
  status: true,
  details: null,
  images: []
}

const item = {
  namespaced: true,
  state: {
    search: null,
    item: null,
    items: emptyDataSource,
    errors: null,
    selected: []
  },
  mutations: {
    [NEW] (state) {
      state.item = Object.assign({}, initialValue)
    },
    [ERROR] (state, payload) {
      state.errors = payload
    },
    [SAVE_SUCCESS] (state, payload) {
      state.item = Object.assign(state.item, payload)
      if (state.selected.length === 1 && state.selected[0].id === state.item.id) {
        Object.assign(state.selected[0], state.item)
      }
    },
    [SEARCH_SUCCESS] (state, payload) {
      state.items = payload
    },
    [SELECT] (state, payload) {
      if (state.selected.indexOf(payload) === -1) {
        state.selected.push(payload)
      } else {
        state.selected = state.selected.filter(item => item.id !== payload.id)
      }
    },
    [EDIT] (state, payload) {
      state.item = _.clone(payload)
    },
    [REORDER] (state, payload) {
      state.item.images = payload
      state.item.images.forEach((image, index) => {
        console.log(image.file.name, index)
      })
    }
  },
  actions: {
    _new ({commit}) {
      commit(NEW)
    },
    save ({commit, state}) {
      const images = state.item.images
      const item = _.clone(state.item)
      delete item['images']
      let promise
      let id
      if ('id' in item && item.id) {
        promise = resource.update({id: item.id}, item)
        id = item.id
      } else {
        promise = resource.save({}, item)
      }
      promise.then(response => {
        const imagesSave = images.filter(image => !image.deleted)
        const imagesDelete = images.filter(image => image.deleted && image.id)
        const callbacks = []
        if (id === undefined) {
          id = response.body.id
        }

        imagesSave.forEach(image => {
          callbacks.push(callback => {
            const formData = new FormData()
            if (image.blob) {
              formData.append('file', image.blob)
            }
            formData.append('position', image.position)
            formData.append('foreign_table', 'items')
            formData.append('foreign_id', id)
            formData.append('public', image.public ? '1' : '0')

            let promise
            if ('id' in image && image.id) {
              promise = fileResource.save({id: image.id}, formData)
            } else {
              promise = fileResource.save({}, formData)
            }

            promise.then(response => {
              delete image.blob
              if (!('id' in image)) {
                image.id = response.body.id
              }
              callback()
            },
            error => commit(ERROR, error.body))
          })
        })

        imagesDelete.forEach(image => {
          callbacks.push(callback => {
            fileResource.delete({id: image.id}).then(() => {
              callback()
            },
            error => commit(ERROR, error.body))
          })
        })

        asyncWaterfall(callbacks, () => {
          Notify.info('Produto salvo com sucesso!')
          commit(SAVE_SUCCESS, response.body)
        })
      }, error => commit(ERROR, error.body))
    },
    search ({commit, state}, payload) {
      resource.get(payload).then(response => commit(SEARCH_SUCCESS, response.body), error => commit(ERROR, error.body))
    },
    select ({commit, state}, payload) {
      commit(SELECT, payload)
    },
    edit ({commit, state}, payload) {
      commit(EDIT, payload)
    },
    reorder ({commit, state}, payload) {
      commit(REORDER, payload)
    }
  }
}

export default item
