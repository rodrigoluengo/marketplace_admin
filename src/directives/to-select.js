import Vue from '@/vue'
import $ from 'jquery'

Vue.directive('to-select', {
  inserted (el) {
    $(el).toSelect()
  }
})
