import Vue from 'vue'
import Vuex from 'vuex'
import Router from 'vue-router'
import VueResource from 'vue-resource'

Vue.use(Vuex)
Vue.use(Router)
Vue.use(VueResource)

export default Vue
