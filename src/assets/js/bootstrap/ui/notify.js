import $ from 'jquery'

export default class Notify {
  static messages = null

  static alert (messages, args) {
    this.show(messages, args, 'danger', 'fa-exclamation-triangle')
  }

  static info (messages, args) {
    this.show(messages, args, 'info', 'fa-info-circle')
  }

  static success (messages, args) {
    this.show(messages, args, 'success', 'fa-check')
  }

  static show (messages, args, type, icon) {
    this.messages = messages
    if (typeof this.messages === 'string') {
      this.messages = [this.messages]
    }

    $.each(this.messages, function (i, message) {
      window.setTimeout(function () {
        $.notify({
          icon: 'fa ' + icon,
          message: message
        }, {
          type: type
        })
      }, i * 100)
    })
  }
}
