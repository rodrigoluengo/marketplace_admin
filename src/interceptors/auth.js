import { HttpStatus } from '@/enums/http-status'
import router from '@/router'
import store from '@/store'
import Loading from '@/assets/js/bootstrap/ui/loading'

export default function (request, next) {
  Loading.on()
  if (store.state.auth.auth) {
    request.headers.set('Authorization', `${store.state.auth.auth.token_type} ${store.state.auth.auth.access_token}`)
    request.headers.set('Store', store.state.auth.auth.store)
  }
  next(response => {
    Loading.off()
    if (response.status === HttpStatus.UNAUTHORIZED) {
      store.commit('auth/AUTH_LOGOUT')
      router.push({name: 'Login'})
    }
    if (response.headers.get('authorization')) {
      store.commit('auth/AUTH_REFRESH', response.headers.get('authorization'))
    }
  })
}
