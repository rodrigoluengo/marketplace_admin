import resource from '@/resources/password-reset'
import router from '@/router'

import Notify from '@/assets/js/bootstrap/ui/notify'

import {LOGOUT} from './auth'

export const SAVE_SUCCESS = 'PASSWORD_RESET_SAVE_SUCCESS'
export const UPDATE = 'PASSWORD_RESET_UPDATE'
export const UPDATE_SUCCESS = 'PASSWORD_RESET_UPDATE_SUCCESS'
export const ERROR = 'PASSWORD_RESET_ERROR'

const passwordReset = {
  namespaced: true,
  state: {
    token: null,
    email: null,
    password: null,
    password_confirmation: null,
    errors: null
  },

  mutations: {
    [UPDATE] (state, payload) {
      Object.assign(state, payload)
    },
    [ERROR] (state, payload) {
      state.errors = payload
    }
  },

  actions: {
    save ({commit, rootState}) {
      resource.save({email: rootState.auth.email}).then(response => {
        commit(SAVE_SUCCESS)
        router.push({name: 'Login'})
        Notify.info('Um e-mail foi enviado com as informações para atualizar à senha, verifique sua caixa de entrada')
      }, response => {
        commit(ERROR, response.body)
      })
    },
    update ({commit, state}) {
      resource.update(state).then(response => {
        commit(UPDATE_SUCCESS)
        commit(LOGOUT)
        router.push({name: 'Login'})
        Notify.success('Senha atualizada com sucesso')
      }, response => {
        commit(ERROR, response.body)
      })
    }
  }

}

export default passwordReset
