import $ from 'jquery'

class Dialog {
  static open (params, event) {
    let _params = {
      id: 'Modal',
      title: null,
      icon: '',
      content: '',
      fnOk: null,
      textAlign: 'text-center',
      cancel: null,
      size: '',
      stopPropagation: false
    }

    if (typeof params === 'string') {
      params = {content: params}
    }

    params = {..._params, params}

    let html = `<div id="dialog${params.id}" class="modal" style="display: none">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">`

    if (params.title) {
      html += `<div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                  <h4 class="modal-title" id="dialogAlertTitle">${params.title}</h4>
                </div>`
    }

    html += `<div class="modal-body ${params.textAlign}">`

    if (params.icon) {
      html += `<i class="fa ${params.icon}" style="font-size: 2.5em"></i>&nbsp;`
    }

    html += params.content

    html += `</div>
              <div class="modal-footer text-center" style="padding-bottom: 5px">`

    if (params.cancel) {
      html += `<button type="button" id="btDialog${params.id}Cancel" class="btn btn-default" data-dismiss="modal">
                <i class="fa fa-close"></i>&nbsp;<b>Cancelar</b>
              </button>`
    }

    html += `<button type="button" id="btDialog${params.id}Ok" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;<b>Ok</b></button>
            </div>
          </div>
        </div>
      </div>`

    $(html).appendTo($('body'))

    $('#dialog' + params.id).modal({
      show: true,
      keyboard: false,
      backdrop: 'static'
    })
    .on('hidden.bs.modal', function () {
      $(this).remove()
      if (typeof params.onclose !== 'undefined') {
        params.onclose()
      }
    })

    $('#btDialog' + params.id + 'Ok').click(function () {
      $('#dialog' + params.id).modal('hide')
      if (params.fnOk) {
        params.fnOk()
      }
    })

    if (params.stopPropagation && event) {
      event.cancelBubble = true
    }
  }

  static alert (params, event) {
    if (typeof params === 'string') {
      params = $.extend(true, params, {content: params})
    }
    params = $.extend(true, params, {
      icon: 'fa-exclamation-triangle'
    })
    Dialog.open(params, event)
  }

  static confirm (message, fnConfirm, fnCancel) {
    const html = `<div id="dialogConfirm" class="modal" style="display: none">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-body text-center">
                          <h3><i class="fa fa-question-circle"></i>&nbsp; ${message} </h3>
                        </div>
                        <div class="modal-footer text-center" style="padding-bottom: 5px">
                          <div class="row">
                            <div class="col-md-6 form-group">
                              <button type="button" id="btDialogConfirmYes" class="btn btn-primary btn-block">
                                <i class="fa fa-check"></i>
                                &nbsp;Sim
                              </button>
                            </div>
                          <div class="col-md-6 form-group">
                            <button type="button" id="btDialogConfirmNo" data-dismiss="modal" class="btn btn-default btn-block">
                              <i class="fa fa-close"></i>
                              &nbsp;Não
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>`

    $(html).appendTo($('body'))

    $('#dialogConfirm').modal({
      show: true,
      keyboard: false,
      backdrop: 'static'
    })
    .on('hidden.bs.modal', function () {
      $(this).remove()
    })

    $('#btDialogConfirmYes').click(function () {
      fnConfirm()
      $('#dialogConfirm').modal('hide')
    })

    $('#btDialogConfirmNo').click(function () {
      $('#dialogConfirm').modal('hide')
      if (fnCancel) {
        fnCancel()
      }
    })
    window.setTimeout(function () {
      $('#btDialogConfirmNo').focus()
    }, 50)
  }
}

export default Dialog
