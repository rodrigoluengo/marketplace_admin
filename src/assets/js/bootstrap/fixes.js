import $ from 'jquery'

$(document).on('shown.bs.modal', '.modal', function () {
  const previousModalBackDrop = $('.modal-backdrop:not(:last):visible')
  const previousModal = $('.modal:not(:last):visible')
  const lastModalBackDrop = $('.modal-backdrop:last:visible')
  const lastModal = $(this)
  previousModalBackDrop.hide()

  if (previousModalBackDrop.length) {
    lastModalBackDrop.css('z-index', parseInt(previousModal.css('z-index')) + 1)
    lastModal.css('z-index', parseInt(lastModalBackDrop.css('z-index')) + 1)
  }
  if (!$(this).parent().is('body')) {
    $(this)
      .attr('data-append-to-body', true)
      .data({
        modalBackDrop: lastModalBackDrop,
        parent: $(this).parent()
      }).appendTo('body')
  }
}).on('hidden.bs.modal', '.modal', function () {
  const lastModalBackDrop = $('.modal-backdrop:last')
  lastModalBackDrop.show()
})
