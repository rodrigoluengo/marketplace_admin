import Vue from '@/vue'
import $ from 'jquery'

Vue.filter('numberFormat', (value, decimalPlaces = 2) => {
  return typeof value === 'number' ? $.number(value, decimalPlaces, ',') : null
})
