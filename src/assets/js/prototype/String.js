import $ from 'jquery'

/**
 * Template
 * @param {type} arg0
 * @returns {String.prototype@call;replace}
 */

Object.assign(String.prototype, {
  template: function (arg0) {
    return this.replace(/{([^{}]*)}/g, function (a, b) {
      var r = arg0[b]
      return typeof r === 'string' || typeof r === 'number' ? r : a
    })
  },
  trim: function () {
    return $.trim(this)
  },
  isEmpty: function () {
    return this.trim() === ''
  }
})
