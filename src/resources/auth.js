import Vue from '@/vue'

const AuthResource = Vue.resource(`${process.env.API}/auth{/id}`)

export default AuthResource
