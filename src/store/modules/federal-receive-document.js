import resource from '@/resources/federal-receive-document'

export const CAPTCHA = 'FEDERAL_RECEIVE_DOCUMENT_CAPTCHA'
export const CAPTCHA_SUCCESS = 'FEDERAL_RECEIVE_DOCUMENT_CAPTCHA_SUCCESS'
export const SEARCH_SUCCESS = 'FEDERAL_RECEIVE_DOCUMENT_SEARCH_SUCCESS'
export const ERROR_CAPTCHA = 'FEDERAL_RECEIVE_DOCUMENT_ERROR_CAPTCHA'
export const ERROR = 'FEDERAL_RECEIVE_DOCUMENT_ERROR'

const federalReceiveDocument = {
  namespaced: true,
  state: {
    captcha: {
      image: null,
      cookie: null
    },
    type: null,
    document: null,
    since: null,
    result: null,
    error: null
  },
  mutations: {
    [CAPTCHA] (state, payload) {
      state.type = payload.type
      state.document = payload.document
      state.since = payload.since
    },
    [CAPTCHA_SUCCESS] (state, payload) {
      state.captcha = payload
    },
    [SEARCH_SUCCESS] (state, payload) {
      state.result = payload
    },
    [ERROR_CAPTCHA] (state, payload) {
      state.error = payload
    },
    [ERROR] (state, payload) {
      state.error = payload
    }
  },
  actions: {
    captcha ({commit, state}, payload) {
      commit(CAPTCHA, payload)
      resource.get(payload).then(response => commit(CAPTCHA_SUCCESS, response.body), error => commit(ERROR_CAPTCHA, error.body))
    },
    search ({commit, state}, payload) {
      const data = {
        type: state.type,
        document: state.document,
        since: state.since,
        cookie: state.captcha.cookie,
        captcha_text: payload.captcha_text
      }
      resource.save(data).then(response => commit(SEARCH_SUCCESS, response.body), error => commit(ERROR, error.body))
    }
  }
}

export default federalReceiveDocument
