import $ from 'jquery'
$.fn.extend({

  toSelect: function () {
    return this.each(function () {
      const scope = $(this)
      window.setTimeout(function () { scope.select() }, 100)
    })
  }

})
