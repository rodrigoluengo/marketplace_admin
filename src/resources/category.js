import Vue from '@/vue'

const CategoryResource = Vue.resource(`${process.env.API}/category{/id}`, {}, {
  jstree: {method: 'GET', url: `${process.env.API}/categories/jstree`},
  move: {method: 'PUT', url: `${process.env.API}/categories/move`}
})

export default CategoryResource
