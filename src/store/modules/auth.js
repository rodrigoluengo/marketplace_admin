import resource from '@/resources/auth'
import router from '@/router'

export const SUCCESS = 'AUTH_SUCCESS'
export const REFRESH = 'AUTH_REFRESH'
export const LOGOUT = 'AUTH_LOGOUT'
export const ERROR = 'AUTH_ERROR'

const auth = {
  namespaced: true,
  state: {
    auth: null,
    email: null,
    password: null,
    remember: false,
    errors: null
  },

  mutations: {
    [SUCCESS] (state, payload) {
      state.auth = payload
    },
    [REFRESH] (state, payload) {
      state.auth = {...state.auth, access_token: payload.split(' ')[1]}
    },
    [LOGOUT] (state) {
      state.auth = null
    },
    [ERROR] (state, payload) {
      state.errors = payload
    }
  },

  actions: {
    login ({state, commit}) {
      resource.save(state).then(response => {
        commit(SUCCESS, response.body)
        router.push({name: 'Dashboard'})
      }, response => {
        commit(ERROR, response.body)
      })
    },

    logout ({commit}) {
      resource.delete().then(() => {
        commit(LOGOUT)
        router.push({name: 'Login'})
      })
    }
  }
}

export default auth
