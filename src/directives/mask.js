import Vue from '@/vue'
import $ from 'jquery'

Vue.directive('mask', {
  bind (el, binding, vnode) {
    switch (binding.value) {
      case 'phone':
        $(el).setMask({mask: '(99) 9999-99999', autoTab: false}).keyup(function (event) {
          let target, phone, element
          target = (event.currentTarget) ? event.currentTarget : event.srcElement
          phone = target.value.replace(/\D/g, '')
          element = $(target)
          element.unsetMask()
          if (phone.length > 10) {
            element.setMask({mask: '(99) 99999-9999', autoTab: false})
          } else {
            element.setMask({mask: '(99) 9999-99999', autoTab: false})
          }
        })
        break
      default:
        $(el).setMask(binding.value)
    }
  }
})
