import Vue from '@/vue'

export const storage = process.env.STORAGE

const FileResource = Vue.resource(`${process.env.API}/file{/id}`)

export default FileResource
