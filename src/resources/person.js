import Vue from '@/vue'

const PersonResource = Vue.resource(`${process.env.API}/person{/id}`)

export default PersonResource
