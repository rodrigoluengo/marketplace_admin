import Vue from '@/vue'

const ItemResource = Vue.resource(`${process.env.API}/item{/id}`)

export default ItemResource
