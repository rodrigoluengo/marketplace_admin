import _ from 'lodash'
import emptyDataSource from '@/assets/js/data/empty-data-source'
import resource from '@/resources/person'
import { initialValue as addressInitialValue } from '@/store/modules/address'

export const NEW = 'PERSON_NEW'
export const ERROR = 'PERSON_ERROR'
export const SAVE_SUCCESS = 'PERSON_SAVE_SUCCESS'
export const SELECT = 'PERSON_SELECT'
export const EDIT = 'PERSON_EDIT'
export const REMOVE_SUCCESS = 'PERSON_REMOVE_SUCCESS'
export const SEARCH_SUCCESS = 'PERSON_SEARCH_SUCCESS'
export const ADD_ADDRESS = 'PERSON_ADD_ADDRESS'
export const REMOVE_ADDRESS = 'PERSON_REMOVE_ADDRESS'
export const ADD_PHONE = 'PERSON_ADD_PHONE'
export const REMOVE_PHONE = 'PERSON_REMOVE_PHONE'

export const phoneInitialValue = {
  name: 'Principal',
  ddd: '',
  number: '',
  branch: '',
  default: true
}

export const initialValue = {
  type: 'F',
  status: true,
  gender: 'M',
  addresses: [_.clone(addressInitialValue)],
  phones: [_.clone(phoneInitialValue)]
}

const person = {
  namespaced: true,
  state: {
    search: null,
    person: _.clone(initialValue),
    people: emptyDataSource,
    errors: null,
    selected: []
  },

  mutations: {
    [SEARCH_SUCCESS] (state, payload) {
      state.people = payload
    },
    [SELECT] (state, payload) {
      if (state.selected.filter(person => person === payload).length === 0) {
        state.selected.push(payload)
      } else {
        state.selected = state.selected.filter(person => person !== payload)
      }
    },
    [EDIT] (state, payload) {
      state.person = _.clone(payload)
    },
    [NEW] (state) {
      const newPerson = _.clone(initialValue)
      state.person = newPerson
    },
    [ADD_ADDRESS] (state) {
      state.person.addresses.push(_.assign(_.clone(addressInitialValue), {
        'default': state.person.addresses.filter(address => address.default).length === 0,
        name: 'Outro endereço ' + state.person.addresses.length
      }))
    },
    [REMOVE_ADDRESS] (state, payload) {
      state.person.addresses = state.person.addresses.filter((address, index) => {
        return index !== payload
      })
    },
    [ADD_PHONE] (state) {
      state.person.phones.push(_.assign(_.clone(phoneInitialValue), {
        'default': state.person.phones.filter(phone => phone.default).length === 0,
        name: 'Outro telefone ' + state.person.phones.length
      }))
    },
    [REMOVE_PHONE] (state, payload) {
      state.person.phones = state.person.phones.filter((phone, index) => {
        return index !== payload
      })
    },
    [ERROR] (state, payload) {
      state.errors = payload
    },
    [SAVE_SUCCESS] (state, payload) {
      state.person = payload
    }
  },
  actions: {
    search ({commit, state}, payload) {
      resource.get(payload).then(response => commit(SEARCH_SUCCESS, response.body), error => commit(ERROR, error.body))
    },
    select ({commit, state}, payload) {
      commit(SELECT, payload)
    },
    edit ({commit, state}, payload) {
      commit(EDIT, payload)
    },
    _new ({commit}) {
      commit(NEW)
    },
    addAddress ({commit}) {
      commit(ADD_ADDRESS)
    },
    removeAddress ({commit}, payload) {
      commit(REMOVE_ADDRESS, payload)
    },
    addPhone ({commit}) {
      commit(ADD_PHONE)
    },
    removePhone ({commit}, payload) {
      commit(REMOVE_PHONE, payload)
    },
    save ({commit, state}) {
      let promise
      if ('id' in state.person && state.person.id) {
        promise = resource.update({id: state.person.id}, state.person)
      } else {
        promise = resource.save({}, state.person)
      }
      promise.then(response => {
        commit(SAVE_SUCCESS, response.body)
      }, error => {
        commit(ERROR, error.body)
      })
    }
  }
}

export default person
