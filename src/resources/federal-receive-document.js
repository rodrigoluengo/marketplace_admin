import Vue from '@/vue'

const FederalReceiveDocumentResource = Vue.resource(`${process.env.API}/federal-receive-document`)

export default FederalReceiveDocumentResource
