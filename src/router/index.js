import Router from 'vue-router'
import $ from 'jquery'
import * as moment from 'moment-timezone'
import Login from '../components/auth/Login'
import ForgotPassword from '../components/password-reset/ForgotPassword'
import PasswordReset from '../components/password-reset/PasswordReset'

import Dashboard from '../components/Dashboard'
import CategorySearch from '../containers/category/Search'
import ItemSearch from '../containers/item/Search'
import PersonSearch from '../containers/person/Search'

const router = new Router({
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/forgot-password',
      name: 'ForgotPassword',
      component: ForgotPassword
    },
    {
      path: '/password-reset/:token',
      name: 'PasswordReset',
      component: PasswordReset
    },
    {
      path: '/',
      name: 'Dashboard',
      component: Dashboard,
      beforeEnter: AuthenticatedGuard
    },
    {
      path: '/categories',
      name: 'CategorySearch',
      component: CategorySearch,
      beforeEnter: AuthenticatedGuard
    },
    {
      path: '/products',
      name: 'ItemSearch',
      component: ItemSearch,
      beforeEnter: AuthenticatedGuard
    },
    {
      path: '/clients',
      name: 'PersonSearch',
      component: PersonSearch,
      beforeEnter: AuthenticatedGuard
    }
  ]
})

router.beforeEach((to, from, next) => {
  let marketplace = localStorage.getItem('marketplace')
  if (marketplace !== null) {
    marketplace = JSON.parse(marketplace)
    if ('auth' in marketplace) {
      const auth = marketplace['auth']['auth']
      if (auth !== null) {
        const timezone = auth['timezone']['timezone']
        const datetimeExpiresIn = moment.tz(auth['datetime_expires_in'], 'YYYY-MM-DD HH:mm', timezone)
        const expiresIn = auth['expires_in']
        if (datetimeExpiresIn.add(expiresIn, 's').isBefore(moment.tz())) {
          router.app.$store.dispatch(`auth/logout`)
        }
      }
    }
  }

  $('.modal[data-append-to-body]').each(function () {
    $(this).modal('hide').appendTo($(this).data('parent'))
  })
  $('.note-popover').remove()
  if (to.path === '/logout') {
    router.app.$store.dispatch(`auth/logout`)
  } else {
    next()
  }
})

export function AuthenticatedGuard (to, from, next) {
  if (router.app.$store.state.auth.auth === null) {
    router.app.$store.state.router.to = to
    router.push({name: 'Login'})
  } else {
    next()
  }
}

export default router
