import resource from '@/resources/address'
import { Notify } from '@/assets/js/bootstrap/ui'

export const SEARCH = 'ADDRESS_SEARCH'
export const SEARCH_SUCCESS = 'ADDRESS_SEARCH_SUCCESS'
export const ERROR = 'ADDRESS_ERROR'

export const initialValue = {
  name: 'Principal',
  'default': true
}

const address = {
  namespaced: true,
  state: {
    search: null,
    address: initialValue,
    errors: null
  },
  mutations: {
    [SEARCH] (state, payload) {
      state.search = payload
    },
    [SEARCH_SUCCESS] (state, payload) {
      state.address = payload
    },
    [ERROR] (state, payload) {
      state.errors = payload
    }
  },

  actions: {
    _search ({commit}, payload) {
      commit(SEARCH, payload)
      resource.zipCode({zipcode: payload}).then(response => {
        commit(SEARCH_SUCCESS, response.body)
        Notify.info('CEP encontrado ')
      }, error => {
        commit(ERROR, error.body)
      })
    }
  }
}

export default address
